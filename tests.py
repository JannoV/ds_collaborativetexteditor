from change import *
import unittest

class ChangesTests(unittest.TestCase):
    def testMovingWriteChanges1(self):
        testMovingConcurrentChanges(self,
                                    Change(1, Change.WriteChange, 1, "a"),
                                    Change(2, Change.WriteChange, 2, "b"))

    def testMovingWriteChanges2(self):
        testMovingConcurrentChanges(self,
                                    Change(1, Change.WriteChange, 2, "a"),
                                    Change(2, Change.WriteChange, 2, "b"))

    def testMovingWriteChanges3(self):
        testMovingConcurrentChanges(self,
                                    Change(1, Change.WriteChange, 4, "a"),
                                    Change(2, Change.WriteChange, 2, "b"))

    def testMovingDeleteChanges1(self):
        testMovingConcurrentChanges(self,
                                    Change(1, Change.DeleteChange, 1, 1),
                                    Change(2, Change.DeleteChange, 2, 1))

    def testMovingDeleteChanges2(self):
        testMovingConcurrentChanges(self,
                                    Change(1, Change.DeleteChange, 2, 1),
                                    Change(2, Change.DeleteChange, 2, 1))

    def testMovingDeleteChanges3(self):
        testMovingConcurrentChanges(self,
                                    Change(1, Change.DeleteChange, 4, 1),
                                    Change(2, Change.DeleteChange, 2, 1))

    def testMovingWriteAndDeleteChange1(self):
        testMovingConcurrentChanges(self,
                                    Change(1, Change.WriteChange, 1, "a"),
                                    Change(2, Change.DeleteChange, 2, 1))

    def testMovingWriteAndDeleteChange2(self):
        testMovingConcurrentChanges(self,
                                    Change(1, Change.WriteChange, 2, "a"),
                                    Change(2, Change.DeleteChange, 2, 1))

    def testMovingWriteAndDeleteChange3(self):
        testMovingConcurrentChanges(self,
                                    Change(1, Change.WriteChange, 3, "a"),
                                    Change(2, Change.DeleteChange, 2, 1))

    def testMovingWriteAndDeleteChange4(self):
        testMovingConcurrentChanges(self,
                                    Change(2, Change.WriteChange, 1, "a"),
                                    Change(1, Change.DeleteChange, 2, 1))

    def testMovingWriteAndDeleteChange5(self):
        testMovingConcurrentChanges(self,
                                    Change(2, Change.WriteChange, 2, "a"),
                                    Change(1, Change.DeleteChange, 2, 1))

    def testMovingWriteAndDeleteChange6(self):
        testMovingConcurrentChanges(self,
                                    Change(2, Change.WriteChange, 3, "a"),
                                    Change(1, Change.DeleteChange, 2, 1))

    def testMovingMultipleConcurrentWriteChanges(self):
        testMovingMultipleConcurrentChanges(self,
            [Change(1, Change.WriteChange, 3, "a"), Change(2, Change.WriteChange, 3, "b")],
            [Change(3, Change.WriteChange, 3, "c"), Change(3, Change.WriteChange, 4, "d")])

    def testMovingMultipleConcurrentDeleteChanges(self):
        testMovingMultipleConcurrentChanges(self,
            [Change(1, Change.DeleteChange, 3, 1), Change(2, Change.DeleteChange, 3, 1)],
            [Change(3, Change.DeleteChange, 3, 1), Change(3, Change.DeleteChange, 4, 1)])

    def testMovingMultipleConcurrentMixedChanges(self):
        testMovingMultipleConcurrentChanges(self,
            [Change(1, Change.WriteChange, 3, "a"), Change(2, Change.DeleteChange, 3, 1)],
            [Change(3, Change.DeleteChange, 3, 1), Change(3, Change.WriteChange, 4, "d")])

def testMovingConcurrentChanges(testsClass, change1, change2):
    text = "ijklmnopqrstuvwxyz1234567890"
    testsClass.assertEqual(applyConcurrentChanges(text, change1, change2),
                           applyConcurrentChanges(text, change2, change1))

def applyConcurrentChanges(text, change1, change2):
    text = applyChange(text, change1)
    c1, c2 = moveChange(change2, change1)
    if c1 != None:
        text = applyChange(text, c1)
    if c2 != None:
        text = applyChange(text, c2)
    return text

def testMovingMultipleConcurrentChanges(testsClass, changes1, changes2):
    text = "ijklmnopqrstuvwxyz1234567890"
    text += text + text + text
    processedChanges1, processedChanges2 = processConcurrentChanges(changes1, changes2)
    testsClass.assertEqual(applyChanges(applyChanges(text, changes1), processedChanges2),
                           applyChanges(applyChanges(text, changes2), processedChanges1))

if __name__ == '__main__':
    unittest.main()