import threading
import time
from collections import deque

from change import *


class FileUpdater:
    def __init__(self, fileName):
        self.fileName = fileName
        self.changes = deque()
        self.lock = threading.Lock()
        threading.Timer(30, self.repeatCleaning).start()

    def repeatCleaning(self):
        while True:
            time.sleep(60)
            self.writeChangesAndCleanQueue()

    def writeChangesAndCleanQueue(self):
        changesToWrite = []
        self.lock.acquire()
        cutoffTime = getTicks() - 11 * 60 * 1000 * 1000 * 10
        while len(self.changes) > 0 and self.changes[0].changeTime < cutoffTime:
            changesToWrite.append(self.changes.popleft())
        self.lock.release()
        f = open(self.fileName, "r")
        content = f.read()
        f.close()
        content = applyChanges(content, changesToWrite)
        f = open(self.fileName, "w")
        f.write(content)
        f.close()

    def getContent(self):
        self.lock.acquire()
        currentChangeToken = getTicks()
        f = open(self.fileName, "a+")
        print "[DEBUG] Get Content Request. Changes: " + str(self.changes)
        content = applyChanges(f.read(), self.changes)
        f.close()
        self.lock.release()
        return currentChangeToken, content

    def addChanges(self, previousChangeToken, changes):
        if getTicks() - previousChangeToken >= 10 * 60 * 1000 * 1000 * 10:
            return None
        self.lock.acquire()
        print "[DEBUG] Adding changes: " + str(changes)
        #print "[DEBUG] All changes: " + str(self.changes)

        laterChanges = []
        for change in reversed(self.changes):
            if change.changeTime <= previousChangeToken:
                break
            laterChanges.append(change)
        #print "[DEBUG] Later changes: " + str(laterChanges)

        changes, laterChanges = processConcurrentChanges(changes, list(reversed(laterChanges)));

        #print "[DEBUG] Later changes (after): " + str(laterChanges)
        currentChangeToken = getTicks()
        for change in changes:
            change.changeTime = currentChangeToken
            self.changes.append(change)

        self.lock.release()
        return currentChangeToken, laterChanges

def getTicks():
    return int(time.clock() * 1000 * 1000 * 10)