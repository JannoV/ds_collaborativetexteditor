from common import *
import random
import string
from change import parseChangeMessage
from change import writeChangeMessage
from fileUpdater import FileUpdater

def __disconnect(sock):
    try:
        sock.fileno()
    except:
        return
    sock.close()

__clientIDs = []
__clientIDtoFile = {}
__fileUpdatersByFileName = {}

def generate_clientID():
    return "CLIENT_" + "".join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))

def process_request(message):
    request_code,clientID,request = message.split(MSG_FIELD_SEP)
    if (request_code == REQ_NEWSESS):
        clientID = generate_clientID()
        __clientIDs.append(clientID)
        return MSG_FIELD_SEP.join((RSP_OK,clientID))
    elif (request_code == REQ_FILE):
        fileName = request
        fileUpdater = __fileUpdatersByFileName.get(fileName)
        if fileUpdater == None:
            fileUpdater = FileUpdater(fileName)
            __fileUpdatersByFileName[fileName] = fileUpdater
        __clientIDtoFile[clientID] = request
        time, content = fileUpdater.getContent()
        return MSG_FIELD_SEP.join((RSP_OK, str(time), content))
    elif request_code == REQ_CHANGE or request_code == REQ_UPDATE:
        fileName = __clientIDtoFile[clientID]
        if request_code == REQ_UPDATE:
            changeTime = int(request)
            changes = []
        else:
            changeTime, changes = parseChangeMessage(request)
        fileUpdater = __fileUpdatersByFileName.get(fileName)
        if fileUpdater == None:
            fileUpdater = FileUpdater(fileName)
            __fileUpdatersByFileName[fileName] = fileUpdater
        currentChangeToken, laterChanges = fileUpdater.addChanges(changeTime, changes)
        return MSG_FIELD_SEP.join((RSP_OK,writeChangeMessage(currentChangeToken, laterChanges)))
    elif (request_code == REQ_CLOSESESS):
        __clientIDs.remove(clientID)
        return MSG_FIELD_SEP.join((RSP_OK,""))
    elif (request_code == REQ_TEST):
        return MSG_FIELD_SEP.join((RSP_OK,"")) if (clientID in __clientIDs) else MSG_FIELD_SEP.join((RSP_ERR,""))
    else:
        return MSG_FIELD_SEP.join((RSP_ERR,""))