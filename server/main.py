from socket import socket, AF_INET, SOCK_STREAM
from common import *
import protocol as sp
import sys

reload(sys)
sys.setdefaultencoding('utf8')

if __name__ == '__main__':
    __server_sock = socket(AF_INET,SOCK_STREAM)
    try:
        __server_sock.bind((DEFAULT_SERVER_IP,DEFAULT_SERVER_PORT))
        print "[INFO] Server has been started on " + DEFAULT_SERVER_IP + ":" + str(DEFAULT_SERVER_PORT)
    except:
        exit(1)
    __server_sock.listen(10)
    __client_sock = None
    while 1:
        __client_sock,source = __server_sock.accept()
        request = None
        try:
            request = receive(__client_sock)
        except:
            sp.__disconnect(__client_sock)
            __client_sock = None
            continue
        response = sp.process_request(request)
        try:
            send(__client_sock, response)
        except:
            sp.__disconnect(__client_sock)
            __client_sock = None
            continue
        sp.__disconnect(__client_sock)
        __client_sock=None
        print "[DEBUG] Request " + request + " received, response " + response + " sent"