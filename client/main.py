import Tkinter
from ScrolledText import *
import tkMessageBox
import tkSimpleDialog
from common import *
import protocol as cp
from change import *
import threading
import time
import sys

reload(sys)
sys.setdefaultencoding('utf8')

root = Tkinter.Tk(className="Collaborative Text Editor")
#The functions are same as text of Tkinter http://www.tkdocs.com/tutorial/text.html
textPad = ScrolledText(root, width=100, height=40)

global clientID, server, previousRequestTime, changes, lock
clientID = None
server = None
previousRequestTime = None
changes = []
lock = None

def open_command():
    global clientID, server, previousRequestTime, changes, lock
    defaultAddress = DEFAULT_SERVER_IP + ":" + str(DEFAULT_SERVER_PORT)
    address = tkSimpleDialog.askstring("Server address", "Server address (defaults to " + defaultAddress + ")")
    if address == "":
        address = defaultAddress
    addressSplit = address.split(":")
    if len(addressSplit) == 2:
        fileChangeLock.acquire()
        ip,port = addressSplit;
        server = (ip,int(port))
        clientID = cp.createSession(server)
        fileName = tkSimpleDialog.askstring("Filename", "Name of file to open")
        previousRequestTime, fileContent = cp.getFile(server, clientID, fileName)
        changes = []
        textPad.config(state="normal")
        textPad.delete("1.0", "end")
        textPad.insert("1.0", fileContent)
        fileChangeLock.release()
    else:
        print("Faulty input!")

def exit_command():
    if tkMessageBox.askokcancel("Quit", "Do you really want to quit?"):
        global clientID
        global server
        if clientID != None and server != None:
            cp.closeSession(server, clientID)
        root.destroy()

def about_command():
    tkMessageBox.showinfo("About", "Collaborative Text Editor \n Created by: \n Karl-Kristjan Luberg \n Janno Veeorg \n Liisi Kerik \n Krister Jaanhold")

def text_changed(event):
    global clientID, server, previousRequestTime, changes, lock
    index = tkinterIndexToServerIndex(textPad.index("insert"))
    symbol = event.char
    # Backspace or Delete
    if event.keycode == 8 or event.keycode == 46:
        if event.keycode == 8:
            index -= 1
        lock.acquire()
        changes.append(Change(previousRequestTime, Change.DeleteChange, index, 1))
        lock.release()
        return
    # Enter
    if event.keycode == 13:
        symbol = "\n"
    if symbol != None and symbol != "":
        lock.acquire()
        changes.append(Change(previousRequestTime, Change.WriteChange, index, symbol))
        lock.release()

def tkinterIndexToServerIndex(index):
    line, char = index.split(".")
    line = int(line)
    serverIndex = int(char)
    for i in range(1, line):
        serverIndex += int(textPad.index(str(i) + ".end").split(".")[1]) + 1
    print "[DEBUG] Tkinter index: " + str(index) + "    Server index: " + str(serverIndex)
    return serverIndex

def serverIndexToTkinterIndex(index):
    return textPad.index("1.0+"+str(index)+"c")

def repeatServerCommunication():
    while True:
        time.sleep(3)
        fileChangeLock.acquire()
        communicateWithServer()
        fileChangeLock.release()

def communicateWithServer():
    global clientID, server, previousRequestTime, changes, lock
    lock.acquire()
    print "[DEBUG] Starting communication with server. Changes " + str(changes)
    changesToSend = changes
    changes = []
    lock.release()
    if len(changesToSend) == 0:
        previousRequestTime, changesFromServer = cp.getUpdates(server, clientID, previousRequestTime)
    else:
        previousRequestTime, changesFromServer = cp.addChanges(server, clientID, previousRequestTime, changesToSend)
    lock.acquire()
    changes, changesFromServer = processConcurrentChanges(changes, changesFromServer)
    for change in changesFromServer:
        index = serverIndexToTkinterIndex(change.index)
        if change.changeType == Change.DeleteChange:
            textPad.delete(index, serverIndexToTkinterIndex(change.index + change.argument))
        else:
            textPad.insert(index, change.argument)
    lock.release()


menu = Tkinter.Menu(root)
root.config(menu=menu)
menu.add_command(label="Open connection", command=open_command)
menu.add_command(label="About", command=about_command)
menu.add_command(label="Exit", command=exit_command)
textPad.pack()
textPad.config(state="disabled")

textPad.bind("<Key>", text_changed)
textPad.bind('<Control-x>', lambda e: 'break')
textPad.bind('<Control-c>', lambda e: 'break')
textPad.bind('<Control-v>', lambda e: 'break')
textPad.bind('<Control-a>', lambda e: 'break')
textPad.bind('<Button-3>', lambda e: 'break')
lock = threading.Lock()
fileChangeLock = threading.Lock()
threading.Timer(2, repeatServerCommunication).start()

root.mainloop()