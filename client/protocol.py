from socket import socket, AF_INET, SOCK_STREAM
from common import *
from change import parseChangeMessage
from change import writeChangeMessage

def __disconnect(sock):
    try:
        sock.fileno()
    except sock.error:
        return
    sock.close()

def __request(server,request_type,clientID,request):
    sock = socket(AF_INET,SOCK_STREAM)
    try:
        sock.connect(server)
    except:
        return RSP_ERR,"Couldn't open socket to server!"
    raw_request = MSG_FIELD_SEP.join((request_type,clientID,request))
    try:
        send(sock, raw_request)
    except:
        __disconnect(sock)
        return RSP_ERR,"Couldn't send request to server!"
    raw_response = None
    try:
        raw_response = receive(sock)
    except:
        __disconnect(sock)
        return RSP_ERR,"Couldn't receive response from server!"
    response_type,response = raw_response.split(MSG_FIELD_SEP, 1)
    return response_type,response

def createSession(server):
    response_code,clientID = __request(server, REQ_NEWSESS, "", "")
    return clientID if response_code == RSP_OK else "Failed to open session"

def getFile(server, clientID, fileName):
    response_code,response = __request(server, REQ_FILE, clientID, fileName)
    time, fileContent = response.split(MSG_FIELD_SEP, 1)
    return (int(time), fileContent) if response_code == RSP_OK else "Failed to get file"

def addChanges(server, clientID, previousRequestTime, changes):
    response_code,updates = __request(server, REQ_CHANGE, clientID, writeChangeMessage(previousRequestTime, changes))
    return parseChangeMessage(updates) if response_code == RSP_OK else "Change error"

def getUpdates(server, clientID, previousRequestTime):
    response_code,updates = __request(server, REQ_UPDATE, clientID, str(previousRequestTime))
    return parseChangeMessage(updates) if response_code == RSP_OK else "Failed to get updates"

def closeSession(server, clientID):
    response_code,_ = __request(server, REQ_CLOSESESS, clientID, "")
    return "Session " + clientID + " closed" if response_code == RSP_OK else "Failed to close session"

def test(server, clientID):
    response_code,_ = __request(server, REQ_TEST, clientID, "")
    return "Test success" if response_code == RSP_OK else "Test fail"