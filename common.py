from socket import SHUT_WR

#Default ip:port
DEFAULT_SERVER_IP = "127.0.0.1"
DEFAULT_SERVER_PORT = 8080
#Requests
REQ_NEWSESS = "0"
REQ_FILE = "1"
REQ_CHANGE = "2"
REQ_UPDATE = "4"
REQ_CLOSESESS = "5"
REQ_TEST = "6"
REQ_DESC = {
    REQ_NEWSESS:"Create new session",
    REQ_FILE:"Create new file or open existing file",
    REQ_CHANGE:"Send change to server and request changes from server",
    REQ_UPDATE:"Request updates from server",
    REQ_CLOSESESS:"Close existing session",
    REQ_TEST:"Simple ping used for testing the application"
}
#Responses
RSP_OK = "0"
RSP_ERR = "1"
RSP_DESC = {
    RSP_OK:"No error",
    RSP_ERR:"Error"
}
#Separator
MSG_FIELD_SEP = "::"
#Buffersize for how big blocks of message to receive at once
BUFFERSIZE = 4096

def send(sock,message):
    sock.sendall(message)
    #THIS SHUT_WR IS SUPER IMPORTANT, ELSE SOCK.RECV WILL FOREVER RECEIVE
    sock.shutdown(SHUT_WR)

def receive(sock):
    message = ""
    while 1:
        block = sock.recv(BUFFERSIZE)
        if len(block) == 0:
            break
        message += block
    return message