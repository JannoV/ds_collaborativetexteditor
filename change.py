class Change:
    WriteChange = 0
    DeleteChange = 1

    def __init__(self, changeTime, changeType, index, argument):
        self.changeTime = changeTime
        self.changeType = changeType
        self.index = index
        self.argument = argument

    def __str__(self):
        argument = unicode(self.argument).encode("utf-8") if self.changeType == Change.WriteChange else str(self.argument)
        return str(self.changeTime) + "|" + str(self.changeType) + "|" + str(self.index) + "|" + argument

    def __repr__(self):
        return self.__str__()

def parseChangeMessage(messageString):
    changeStrings = messageString.split("__")
    changeTime = int(changeStrings[0])
    del changeStrings[0]
    changes = []
    for changeString in changeStrings:
        changes.append(getChangeFromString(changeString.replace("_ ", "_")))
    return int(changeTime), changes

def getChangeFromString(changeString):
    changeTime, changeType, index, argument = changeString.split("|", 3)
    if int(changeType) == Change.DeleteChange:
        argument = int(argument)
    else:
        argument = argument.decode("utf-8")
    return Change(int(changeTime), int(changeType), int(index), argument)

def writeChangeMessage(changeTime, changes):
    result = [str(changeTime)]
    for change in changes:
        result.append(str(change).replace("_", "_ "))
    return "__".join(result)

def processConcurrentChanges(firstChanges, secondChanges):
    return moveChanges(firstChanges, secondChanges), moveChanges(secondChanges, firstChanges)

def moveChanges(changesToMove, stayingChanges):
    newChanges = changesToMove
    for stayingChange in stayingChanges:
        newChanges = moveChangesByOne(newChanges, stayingChange)
    return newChanges

def moveChangesByOne(changesToMove, stayingChange):
    if len(changesToMove) == 0:
        return []
    newChangesToMove = []
    for i in range(1, len(changesToMove)):
        newChangesToMove.append(changesToMove[i])
    x, y = moveChange(stayingChange, changesToMove[0])
    if x != None:
        newChangesToMove = moveChangesByOne(newChangesToMove, x)
    if y != None:
        newChangesToMove = moveChangesByOne(newChangesToMove, y)
    x, y = moveChange(changesToMove[0], stayingChange)
    if y != None:
        newChangesToMove.insert(0, y)
    if x != None:
        newChangesToMove.insert(0, x)
    return newChangesToMove

def moveChange(changeToMove, stayingChange):
    newChange = Change(changeToMove.changeTime, changeToMove.changeType, changeToMove.index, changeToMove.argument)
    if changeToMove.changeType == Change.WriteChange:
        if stayingChange.changeType == Change.WriteChange:
            if changeToMove.index >= stayingChange.index and (changeToMove.changeTime >= stayingChange.changeTime or changeToMove.index != stayingChange.index):
                newChange.index += len(stayingChange.argument)
        else:
            if changeToMove.index > stayingChange.index + stayingChange.argument:
                newChange.index -= stayingChange.argument
            elif changeToMove.index > stayingChange.index:
                newChange.index = stayingChange.index
        return newChange, None
    else:
        if stayingChange.changeType == Change.WriteChange:
            if changeToMove.index >= stayingChange.index:
                newChange.index += len(stayingChange.argument)
            elif changeToMove.index + changeToMove.argument > stayingChange.index:
                newChange.argument = stayingChange.index - changeToMove.index
                newChange2 = Change(changeToMove.changeTime, changeToMove.changeType,
                                    stayingChange.index + len(stayingChange.argument),
                                    changeToMove.argument - newChange.argument)
                return newChange2, newChange
        else:
            if changeToMove.index >= stayingChange.index + stayingChange.argument:
                newChange.index -= stayingChange.argument
            elif changeToMove.index >= stayingChange.index:
                if changeToMove.index + changeToMove.argument <= stayingChange.index + stayingChange.argument:
                    return None, None
                else:
                    newChange.index = stayingChange.index + stayingChange.argument
                    newChange.argument = newChange.index - changeToMove.index
            elif changeToMove.index + changeToMove.argument > stayingChange.index:
                if changeToMove.index + changeToMove.argument <= stayingChange.index + stayingChange.argument:
                    newChange.argument -= changeToMove.index + changeToMove.argument - stayingChange.index
                else:
                    newChange.argument = stayingChange.index - changeToMove.index
                    newChange2 = Change(changeToMove.changeTime, changeToMove.changeType,
                                        stayingChange.index + stayingChange.argument,
                                        changeToMove.index + changeToMove.argument - stayingChange.index - stayingChange.argument)
                    return newChange2, newChange
        return newChange, None


def applyChanges(content, changes):
    for change in changes:
        content = applyChange(content, change)
    return content

def applyChange(content, change):
    if change.changeType == Change.WriteChange:
        return content[:change.index] + change.argument + content[change.index:]
    else:
        return content[:change.index] + content[(change.index + change.argument):]